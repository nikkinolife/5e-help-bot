import spell_model.SpellDB;

public class NonCommand {

    SpellDB spellDB;
    public String nonCommandExecute(Long chatId, String userName, String text) {
                String answer;
                spellDB = SpellDB.getInstance();
        try {
            answer = SpellDB.formSpellInfo(SpellDB.findSpell(text));
            //логируем событие, используя userName
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("заклинание не найдено");
            answer = "Заклинание не найдено";
            //логируем событие, используя userName
        }
        return answer;
    }
}
