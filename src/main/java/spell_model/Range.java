package spell_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Range {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("distance")
    @Expose
    private Distance distance;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public String formRangeAnswer() {

        if (distance.getType().equals("self"))
            return "self";
        else return distance.getAmount() + " " +distance.getType();


    }
}
