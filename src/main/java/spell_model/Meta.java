package spell_model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class Meta {

    @SerializedName("ritual")
    @Expose
    private Boolean ritual;

    public Boolean getRitual() {
        return ritual;
    }

    public void setRitual(Boolean ritual) {
        this.ritual = ritual;
    }

}