
package spell_model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Classes {

    @SerializedName("fromClassList")
    @Expose
    private List<FromClassList> fromClassList = null;
    @SerializedName("fromClassListVariant")
    @Expose
    private List<FromClassListVariant> fromClassListVariant = null;

    public List<FromClassList> getFromClassList() {
        return fromClassList;
    }

    public void setFromClassList(List<FromClassList> fromClassList) {
        this.fromClassList = fromClassList;
    }

    public List<FromClassListVariant> getFromClassListVariant() {
        return fromClassListVariant;
    }

    public void setFromClassListVariant(List<FromClassListVariant> fromClassListVariant) {
        this.fromClassListVariant = fromClassListVariant;
    }

}
