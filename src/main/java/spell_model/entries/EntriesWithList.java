package spell_model.entries;

import java.util.List;

public class EntriesWithList extends AbstractEntries {
    private String name;
    private List<String> items;

    public List<String> getList() {
        return items;
    }

    public void setList(List<String> items) {
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}