package spell_model.entries;

import java.util.List;

public class EntriesWithEntries extends AbstractEntries {
    private String name;
    private List<AbstractEntries> entries;

    public List<AbstractEntries> getEntries() {
        return entries;
    }

    public void setEntries(List<AbstractEntries> entries) {
        this.entries = entries;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
