package spell_model.entries;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class EntriesInterfaceDeserializer implements JsonDeserializer<AbstractEntries> {
    @Override
    public AbstractEntries deserialize(JsonElement json,
                                       Type type,
                                       JsonDeserializationContext context) throws JsonParseException {
        //System.out.println(json);
        if (json.isJsonPrimitive()){        // если получили простую строку на входе
                                            // мы хотим сделать из строки энтри со строкой
            EntriesWithString entries = new EntriesWithString();
//            System.out.println(json.getAsString());
//            try {
//                JsonObject jObject = (JsonObject) json;
//                JsonElement objName = jObject.get("name");
//                if (objName!=null){
//                    List<String> array = new ArrayList<>();
//                    array.add(json.getAsString());
//                    entries.setStringEntries(array);
//                    //System.out.println(entries.toString());
//                } else {
//                    System.out.println("тест - " + json.getAsString());
//                    entries.setName("entries with string");
//                    entries.setType("entries");
//                    List<String> array = new ArrayList<>();
//                    array.add(json.getAsString());
//                    entries.setStringEntries(array);
//                    //System.out.println(entries.toString());
//                }
//                return entries;
//            } catch (Exception e){
//                System.out.println("ошибка");
//                //e.printStackTrace();
//            }
            entries.setName("entries with string");
            entries.setType("entries");
            List<String> array = new ArrayList<>();
            array.add(json.getAsString());
            entries.setStringEntries(array);
            //System.out.println(entries.toString());
            return entries;
        }

        JsonObject jObject = (JsonObject) json;
        JsonElement typeObj = jObject.get("type");
        if (typeObj!= null ){
            String typeVal = typeObj.getAsString();

            switch (typeVal){
                case "entries":{
                    //System.out.println(json);
                    return context.deserialize(json, EntriesWithEntries.class);
                }
                case "table":{
                    EntriesWithString entries = new EntriesWithString();
                    entries.setName("entries");
                    List<String> array = new ArrayList<>();
                    array.add("ТУТ БЫЛА ТАБЛИЦА");
                    entries.setStringEntries(array);
                    entries.type = "entries";
                    return entries;
                }
                case "list":{
                    return context.deserialize(json, EntriesWithList.class);
                }

            }
        }
        return null;
    }
}
