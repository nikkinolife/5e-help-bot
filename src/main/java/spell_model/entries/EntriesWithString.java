package spell_model.entries;

import java.util.List;

public class EntriesWithString extends AbstractEntries{
    private String name;

    public List<String> getStringEntries() {
        return stringEntries;
    }

    public void setStringEntries(List<String> stringEntries) {
        this.stringEntries = stringEntries;
    }

    private List<String> stringEntries;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
