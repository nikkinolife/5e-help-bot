package spell_model;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.language.Soundex;
import spell_model.components.Components;
import spell_model.components.ComponentsDeserializer;
import spell_model.entries.AbstractEntries;
import spell_model.entries.EntriesInterfaceDeserializer;
import spell_model.entries.EntriesWithEntries;
import spell_model.entries.EntriesWithString;

import org.apache.commons.text.similarity.LevenshteinDistance;

import javax.xml.parsers.DocumentBuilder;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.*;

public class SpellDB {
    private static SpellDB instance;
    public static List<SpellListFromOneSource> data = new ArrayList<>();

    private SpellDB()  {
        //gson = new Gson();

        getSpellsFromSource("spells-phb.json");
        getSpellsFromSource("spells-ai.json");
        getSpellsFromSource("spells-egw.json");
        getSpellsFromSource("spells-ggr.json");
        getSpellsFromSource("spells-idrotf.json");
        getSpellsFromSource("spells-llk.json");
        getSpellsFromSource("spells-tce.json");
        getSpellsFromSource("spells-xge.json");
    }

    private void getSpellsFromSource(String fileName) {
        try {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(AbstractEntries.class, new EntriesInterfaceDeserializer());
            builder.registerTypeAdapter(Components.class, new ComponentsDeserializer());
            Type ScalingLevelDiceType = new TypeToken<List<ScalingLevelDice>>() {}.getType();
            builder.registerTypeAdapter(ScalingLevelDiceType, new ScalingLevelDiceAdapter());
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(fileName).getFile());
            JsonReader reader = new JsonReader(new FileReader(file));
            Gson gson = builder.create();
            data.add(gson.fromJson(reader, SpellListFromOneSource.class));
            System.out.println("спелл лист получен из " + fileName);
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("ошибка при чтении жсона " + fileName);
        }
    }

    public static SpellDB getInstance()  {
        if (instance==null){
            instance = new SpellDB();
        }
        //System.out.println(data);
        return instance;

    }
    public static Spell findSpell(String name) {
        Map<Spell,Integer> distanceMap = new HashMap<>();
        name=name.toLowerCase(Locale.ROOT);

        for (SpellListFromOneSource spellListFromOneSource: data
             ) {
            for (Spell spell: spellListFromOneSource.getSpells()
                 ) {
                int score = FuzzySearch.weightedRatio(name, spell.getName());
                if (score==100){
                    return spell;
                } else {
                    distanceMap.put(spell, score);
                }
            }
        }
        Map.Entry<Spell, Integer> max=null;
        for (Map.Entry<Spell, Integer> e:distanceMap.entrySet()) {
                if (max == null || max.getValue() < e.getValue()) {
                    max = e;
                }
        }
        if (max.getValue()>70) {
            return max.getKey();
        } else {
            return null;
        }

    }

//    public static Spell findSpell(String name) throws EncoderException {
//        name=name.toLowerCase(Locale.ROOT);
//        Soundex soundex = new Soundex();
//        Map<Spell,Integer> distanceMap = new HashMap<>();
//
//        for (SpellListFromOneSource spellListFromOneSource: data
//             ) {
//            for (Spell spell: spellListFromOneSource.getSpells()
//                 ) {
//                if (soundex.difference(name, spell.getName())==spell.getName().length()){
//                    return spell;
//                } else {
//                    distanceMap.put(spell, soundex.difference(name, spell.getName()));
//                }
//            }
//        }
//        Map.Entry<Spell, Integer> max=null;
//        for (Map.Entry<Spell, Integer> e:distanceMap.entrySet()) {
//                if (max == null || max.getValue() < e.getValue()) {
//                    max = e;
//                }
//            }
//        return max.getKey();
//    }
//    public static Spell findSpell(String name){ // с дистанцией левенштейна
//        name=name.toLowerCase(Locale.ROOT);
//        Map<Spell,Integer> distanceMap = new HashMap<>();
//        LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
//        for (SpellListFromOneSource spellList: data
//             ) {
//            System.out.println("ищем в "  + spellList);
//            for (Spell s:spellList.getSpells()
//            ) {
//                System.out.println("сравниваем с "  + s.getName().toLowerCase(Locale.ROOT));
//                int distance = levenshteinDistance.apply(name, s.getName().toLowerCase(Locale.ROOT));
//                System.out.println("дистанция " + distance);
//                if (distance==0){
//                    System.out.println("дистанция " + distance);
//                    return s;
//                }
//                else {
//                    System.out.println("дистанция при уходе в лист ожидания " + distance);
//                    distanceMap.put(s, distance);
//                }
//            }
////            Map.Entry<Spell, Integer> min = null;             // основная часть для поиска неполного соответствия
//                                                                // (она не работает)
////            for (Map.Entry<Spell, Integer> e:distanceMap.entrySet()) {
////                if (min == null || min.getValue() > e.getValue()) {
////                    min = e;
////                }
////            }
////            return min.getKey();
//        }
//        return null;
//    }

//public static Spell findSpell(String name){  // базовый поиск по имени
//
//        for (Spell s: data.getSpells()
//        ) {
//            if (s.getName().equalsIgnoreCase(name)){
//                System.out.println("заклинание найдено");
//                return s;
//
//            }
//        }
//
//    return null;
//}


    //    public static String formSpellInfo(Spell spell){ // TODO: русификация
    //        String result="";
    //        StringBuilder builder = new StringBuilder(result);
    //        builder.append("<b>Название:</b> ").append(spell.getName()).append("\n");
    //        builder.append("<b>Уровень:</b> ").append(spell.getLevel());
    //        if (spell.getMeta()!=null){
    //            if (spell.getMeta().getRitual())
    //                builder.append(" (ritual)");
    //        }
    //        builder.append("\n");
    //        builder.append("<b>Школа:</b> ").append(schoolEnum.valueOf(spell.getSchool()).getTitle()).append("\n");
    //        builder.append("<b>Время накладывания:</b> ")
    //                .append(spell.getTime().get(0).getNumber())
    //                .append(" ")
    //                .append(spell.getTime().get(0).getUnit())
    //                .append("\n");
    //        builder.append("<b>Компоненты:</b> ");
    //        if (spell.getComponents().getS())
    //            builder.append("S, ");
    //        if (spell.getComponents().getV())
    //            builder.append("V");
    //        if (!spell.getComponents().getM().getText().equals("")){
    //            builder.append(", M (")
    //                    .append(spell.getComponents().getM().getText())
    //                    .append(")");
    //        }
    //        builder.append("\n");
    ////        if (spell.getComponents().getS()!=null) // TODO сложные материальные компоненты
    ////            builder.append(spell.getComponents().getM());
    //        //builder.append("<b>Длительность:</b> ").append(spell.getDuration()).append("\n");
    //        builder.append("<b>Дальность:</b> ").append(spell.getRange().formRangeAnswer()).append("\n");
    //        builder.append("<b>Описание:</b> ");
    //        for (AbstractEntries firstLevelEntrie:spell.getAbstractEntriesList()
    //             ) {
    //            if (firstLevelEntrie instanceof EntriesWithString){
    //                builder.append(((EntriesWithString) firstLevelEntrie).getStringEntries().get(0).replaceAll("[\\[\\]]", ""))
    //                        .append("\n");
    //            } else if (firstLevelEntrie instanceof EntriesWithEntries) {
    //                for (AbstractEntries secondLevelEntrie:((EntriesWithEntries) firstLevelEntrie).getEntries()
    //                     ) {
    //                    if (secondLevelEntrie instanceof EntriesWithString){
    //                        builder.append("<b>")
    //                                .append(((EntriesWithEntries) firstLevelEntrie).getName())
    //                                .append(".</b> ")
    //                                .append(((EntriesWithString) secondLevelEntrie).getStringEntries().get(0).replaceAll("[\\[\\]]", ""))
    //                                .append("\n");
    //                    }
    //                }
    //            }
    //        }

    public static String formSpellInfo(Spell spell){
        String result="";
        StringBuilder builder = new StringBuilder(result);
        builder.append("<b>Name:</b> ").append(spell.getName()).append("\n");
        if (spell.getLevel()==0){
            builder.append("<b>Spell Level:</b> ").append(spell.getLevel())
                    .append(" (Cantrip)");
        } else {
            builder.append("<b>Spell Level:</b> ").append(spell.getLevel());
        }
        if (spell.getMeta()!=null){
            if (spell.getMeta().getRitual())
                builder.append(" (ritual)");
        }
        builder.append("\n");
        builder.append("<b>School:</b> ").append(schoolEnum.valueOf(spell.getSchool()).getTitle()).append("\n");
        builder.append("<b>Casting Time:</b> ")
                .append(spell.getTime().get(0).getNumber())
                .append(" ")
                .append(spell.getTime().get(0).getUnit())
                .append("\n");
        builder.append("<b>Components:</b> ");
        if (spell.getComponents().getS())
            builder.append("S, ");
        if (spell.getComponents().getV())
            builder.append("V");
        if (!spell.getComponents().getM().getText().equals("")){
            builder.append(", M (")
                    .append(spell.getComponents().getM().getText())
                    .append(")");
        }
        builder.append("\n");
//        if (spell.getComponents().getS()!=null) // TODO сложные материальные компоненты
//            builder.append(spell.getComponents().getM());
        //builder.append("<b>Длительность:</b> ").append(spell.getDuration()).append("\n");
        builder.append("<b>Range:</b> ").append(spell.getRange().formRangeAnswer()).append("\n");
        builder.append("<b>Duration:</b> ").append("\n");
        for (AbstractEntries firstLevelEntrie:spell.getAbstractEntriesList()
             ) {
            if (firstLevelEntrie instanceof EntriesWithString){
                builder.append(((EntriesWithString) firstLevelEntrie).getStringEntries().get(0).replaceAll("[\\[\\]]", ""))
                        .append("\n");
            } else if (firstLevelEntrie instanceof EntriesWithEntries) {
                for (AbstractEntries secondLevelEntrie:((EntriesWithEntries) firstLevelEntrie).getEntries()
                     ) {
                    if (secondLevelEntrie instanceof EntriesWithString){
                        builder.append("<b>")
                                .append(((EntriesWithEntries) firstLevelEntrie).getName())
                                .append(".</b> ")
                                .append(((EntriesWithString) secondLevelEntrie).getStringEntries().get(0).replaceAll("[\\[\\]]", ""))
                                .append("\n");
                    }
                }
            }
        }

//        builder.append("\n");
//        builder.append("\n");
        System.out.println("ответ сформирован");
        return builder.toString();

    }


}
