package spell_model.components;

import com.google.gson.*;

import java.lang.reflect.Type;

public class ComponentsDeserializer implements JsonDeserializer<Components> {

    @Override
    public Components deserialize(JsonElement json,
                                              Type type,
                                              JsonDeserializationContext context) throws JsonParseException {
        JsonObject jObject = (JsonObject) json;
        Components components = new Components();
        components.setS(jObject.get("s") != null);
        components.setV(jObject.get("v") != null);
        ComponentsM fullM = new ComponentsM();
        if (jObject.get("m")!=null){
            //System.out.println("есть М компонент");
            if (jObject.get("m").isJsonPrimitive()){
                // "m": "a few grains of sugar, some kernels of grain, and a smear of fat"
                fullM.setText(jObject.get("m").getAsString());
                fullM.setCost(0);
                fullM.setConsume(false);
            } else
                    // "m": {
//                    "text": "a lead-based ink worth at least 10 gp, which the spell consumes",
//                    "cost": 1000
            {
                JsonObject MObj = (JsonObject) jObject.get("m");
                fullM.setText(MObj.get("text").getAsString());
                fullM.setConsume(MObj.get("consumable") != null);
            }
        } else {
            fullM.setConsume(false);
            fullM.setText("");
            fullM.setCost(0);
        }
        components.setM(fullM);
        return components;
    }

}