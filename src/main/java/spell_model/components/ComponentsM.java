package spell_model.components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComponentsM {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("consume")
    @Expose
    private boolean consume;
    @SerializedName("cost")
    @Expose
    private Integer cost;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isConsume() {
        return consume;
    }

    public void setConsume(boolean consume) {
        this.consume = consume;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
