
package spell_model.components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Components {

    @SerializedName("v")
    @Expose
    private Boolean v;
    @SerializedName("s")
    @Expose
    private Boolean s;
    @SerializedName("m")
    @Expose
    private ComponentsM m;

    public ComponentsM getM() {
        return m;
    }

    public void setM(ComponentsM m) {
        this.m = m;
    }

    public Boolean getV() {
        return v;
    }

    public void setV(Boolean v) {
        this.v = v;
    }

    public Boolean getS() {
        return s;
    }

    public void setS(Boolean s) {
        this.s = s;
    }

}
