package spell_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SpellListFromOneSource {
    @SerializedName("spell")
    @Expose
    private List<Spell> spells = null;

    public List<Spell> getSpells() {
        return spells;
    }

    public void setSpell(List<Spell> spells) {
        this.spells = spells;
    }

}
