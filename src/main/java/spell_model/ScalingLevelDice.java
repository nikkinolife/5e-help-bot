package spell_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScalingLevelDice {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("scaling")
    @Expose
    private Scaling scaling;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Scaling getScaling() {
        return scaling;
    }

    public void setScaling(Scaling scaling) {
        this.scaling = scaling;
    }

}
