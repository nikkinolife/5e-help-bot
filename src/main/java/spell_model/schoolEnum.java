package spell_model;

public enum schoolEnum {
    C("Conjuration"),
    N("Necromancy"),
    V("Evocation"),
    A("Abjuration"),
    T("Transmutation"),
    D("Divination"),
    E("Enchantment"),
    I("Illusion");

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;

    schoolEnum(String title) {
        this.title = title;
    }
}
