package spell_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class Scaling {

    @SerializedName("1")
    @Expose
    private String _1;
    @SerializedName("5")
    @Expose
    private String _5;
    @SerializedName("11")
    @Expose
    private String _11;
    @SerializedName("17")
    @Expose
    private String _17;

    public String get1() {
        return _1;
    }

    public void set1(String _1) {
        this._1 = _1;
    }

    public String get5() {
        return _5;
    }

    public void set5(String _5) {
        this._5 = _5;
    }

    public String get11() {
        return _11;
    }

    public void set11(String _11) {
        this._11 = _11;
    }

    public String get17() {
        return _17;
    }

    public void set17(String _17) {
        this._17 = _17;
    }

}