
package spell_model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import spell_model.components.Components;
import spell_model.entries.AbstractEntries;

public class Spell {
    public List<AbstractEntries> getAbstractEntriesList() {
        return abstractEntriesList;
    }

    public void setAbstractEntriesList(List<AbstractEntries> abstractEntriesList) {
        this.abstractEntriesList = abstractEntriesList;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("srd")
    @Expose
    private Boolean srd;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("school")
    @Expose
    private String school;
    @SerializedName("time")
    @Expose
    private List<Time> time = null;
    @SerializedName("range")
    @Expose
    private Range range;
    @SerializedName("components")
    @Expose
    private Components components;
    @SerializedName("duration")
    @Expose
    private List<Duration> duration = null;
    @SerializedName("entries")
    @Expose
    private List<AbstractEntries> abstractEntriesList = null;
    @SerializedName("scalingLevelDice")
    @Expose
    private List<ScalingLevelDice> scalingLevelDice = null;

    public List<ScalingLevelDice> getScalingLevelDice() {
        return scalingLevelDice;
    }

    public void setScalingLevelDice(List<ScalingLevelDice> scalingLevelDice) {
        this.scalingLevelDice = scalingLevelDice;
    }
    @SerializedName("damageInflict")
    @Expose
    private List<String> damageInflict = null;
    @SerializedName("savingThrow")
    @Expose
    private List<String> savingThrow = null;
    @SerializedName("miscTags")
    @Expose
    private List<String> miscTags = null;
    @SerializedName("areaTags")
    @Expose
    private List<String> areaTags = null;
    @SerializedName("classes")
    @Expose
    private Classes classes;
    @SerializedName("backgrounds")
    @Expose
    private List<Background> backgrounds = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Boolean getSrd() {
        return srd;
    }

    public void setSrd(Boolean srd) {
        this.srd = srd;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public List<Time> getTime() {
        return time;
    }

    public void setTime(List<Time> time) {
        this.time = time;
    }

    public Range getRange() {
        return range;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public Components getComponents() {
        return components;
    }

    public void setComponents(Components components) {
        this.components = components;
    }

    public List<Duration> getDuration() {
        return duration;
    }

    public void setDuration(List<Duration> duration) {
        this.duration = duration;
    }

    public List<String> getDamageInflict() {
        return damageInflict;
    }

    public void setDamageInflict(List<String> damageInflict) {
        this.damageInflict = damageInflict;
    }

    public List<String> getSavingThrow() {
        return savingThrow;
    }

    public void setSavingThrow(List<String> savingThrow) {
        this.savingThrow = savingThrow;
    }

    public List<String> getMiscTags() {
        return miscTags;
    }

    public void setMiscTags(List<String> miscTags) {
        this.miscTags = miscTags;
    }

    public List<String> getAreaTags() {
        return areaTags;
    }

    public void setAreaTags(List<String> areaTags) {
        this.areaTags = areaTags;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    public List<Background> getBackgrounds() {
        return backgrounds;
    }

    public void setBackgrounds(List<Background> backgrounds) {
        this.backgrounds = backgrounds;
    }

}
