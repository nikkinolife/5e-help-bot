package spell_model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ScalingLevelDiceAdapter implements JsonDeserializer<List<ScalingLevelDice>> {

    @Override
    public List<ScalingLevelDice> deserialize(JsonElement json,
                                              Type type,
                                              JsonDeserializationContext context) throws JsonParseException {
    List<ScalingLevelDice> vals = new ArrayList<ScalingLevelDice>();
    if (json.isJsonArray()) {
        for (JsonElement e : json.getAsJsonArray()) {
            vals.add((ScalingLevelDice) context.deserialize(e, ScalingLevelDice.class));
        }
    } else if (json.isJsonObject()) {
            vals.add((ScalingLevelDice) context.deserialize(json, ScalingLevelDice.class));
        } else {
            throw new RuntimeException("Unexpected JSON type: " + json.getClass());
        }
        return vals;
    }

}



//private static class MyOtherClassTypeAdapter implements JsonDeserializer<List<MyOtherClass>> {
//    public List<MyOtherClass> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) {
//        List<MyOtherClass> vals = new ArrayList<MyOtherClass>();
//        if (json.isJsonArray()) {
//            for (JsonElement e : json.getAsJsonArray()) {
//                vals.add((MyOtherClass) ctx.deserialize(e, MyOtherClass.class));
//            }
//        } else if (json.isJsonObject()) {
//            vals.add((MyOtherClass) ctx.deserialize(json, MyOtherClass.class));
//        } else {
//            throw new RuntimeException("Unexpected JSON type: " + json.getClass());
//        }
//        return vals;
//    }
//}