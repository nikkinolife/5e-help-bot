package spell_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FromClassListVariant {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("definedInSource")
    @Expose
    private String definedInSource;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDefinedInSource() {
        return definedInSource;
    }

    public void setDefinedInSource(String definedInSource) {
        this.definedInSource = definedInSource;
    }

}
